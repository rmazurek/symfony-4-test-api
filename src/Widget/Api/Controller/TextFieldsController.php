<?php

namespace App\Widget\Api\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Widget\Infrastructure\Repository\TextField;

class TextFieldsController
{
	/**
	 * @Route("/textfield")
	 */
	public function render()
	{
		$textfield = new TextField();
		$textfield->init('jakiś content','tytuł');
		return new JsonResponse($textfield->render());
	}
}
