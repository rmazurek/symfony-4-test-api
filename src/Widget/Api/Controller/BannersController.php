<?php

namespace App\Widget\Api\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Widget\Infrastructure\Repository\Banner;

class BannersController
{
	/**
	 * @Route("/banner")
	 */
	public function render(Request $request)
	{
		$url = $request->get('url');
		$imageUrl = $request->get('imageUrl');
		$title = $request->get('title');
		$banner = new Banner();
		$banner->init($url,$imageUrl,$title);
		return new JsonResponse($banner->render());
	}
}
