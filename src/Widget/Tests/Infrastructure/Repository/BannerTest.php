<?php

// src/Widget/Tests/Domain/Model/TextFieldTest.php
namespace App\Widget\Tests\Infrastructure\Repository;

use PHPUnit\Framework\TestCase;
use App\Widget\Infrastructure\Repository\Banner;

class BannerTest extends TestCase
{
	public function testInit()
	{
		$url = 'some/url';
		$imageUrl = 'image-1.jpg';
		$title = 'Sample title';
		$banner = new Banner();
		$banner->init($url,$imageUrl,$title);
		$data = $banner->getData();
		if(isset($_SERVER['PWD'])){
			$imageUrl = $_SERVER['PWD'].'/public/images/'.$imageUrl;
		}else{
			$imageUrl = $_SERVER['DOCUMENT_ROOT'].'/images/'.$imageUrl;
		}
		$this->assertEquals($url,$data->getUrl());
		$this->assertEquals($imageUrl,$data->getImage()->getUrl());
		$this->assertEquals($title,$data->getTitle());
	}
}
