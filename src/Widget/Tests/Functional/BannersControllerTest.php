<?php

// src/Widget/Tests/BannersControllerTest.php
namespace App\Widget\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BannersControllerTest extends WebTestCase
{
	public function testGetBanner()
	{
		$url = 'google.pl';
		$imageUrl = 'image-1.jpg';
		$title = 'Tytuł';
		$client = static::createClient();
		$client->request('GET','/banner?url='.$url.'&imageUrl='.$imageUrl.'&title='.$title);
		$this->assertEquals(200, $client->getResponse()->getStatusCode());
	}

// To raczej nie zadziała dla fragmentów strony renderowanych jsonem:
// 	public function testBannerUrl()
// 	{
// 		$url = 'google.pl';
// 		$imageUrl = 'image-1.jpg';
// 		$title = 'Tytuł';
// 		$client = static::createClient();
// 		$crawler = $client->request('GET','/banner?url='.$url.'&imageUrl='.$imageUrl.'&title='.$title);
// 		$link = $crawler->filter('a')->eq(0)->link();
// 		$crawler = $client->click($link);
// 	}
}
