<?php

// src/Widget/Tests/Domain/Model/TextFieldTest.php
namespace App\Widget\Tests\Domain\Model;

use PHPUnit\Framework\TestCase;
use App\Widget\Domain\Model\TextField;

class TextFieldTest extends TestCase
{
	public function testSetGetTitle()
	{
		$title = 'testowy tytuł';
		$textField = new TextField();
		$textField->setTitle($title);
		$this->assertEquals($title,$textField->getTitle());
	}
	
	public function testSetGetContent()
	{
		$content = 'testowy content';
		$textField = new TextField();
		$textField->setContent($content);
		$this->assertEquals($content,$textField->getContent());
	}
}
