<?php

// src/Widget/Infrastructure/Repository/Banner.php
namespace App\Widget\Infrastructure\Repository;

use App\Widget\Domain\Model\TextField as TextFieldModel;
use App\Widget\Domain\Repository\TextFieldInterface;
use App\Widget\Infrastructure\Repository\Widget;

/**
 * Widget widget class.
 */
class TextField extends Widget implements TextFieldInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function render()
	{
		$model = $this->model;
		$html = '';
		if($model->getTitle()){
			$html .= '<p>'.$model->getTitle().'</p>';
		}
		$html .= '<p>'.$model->getContent().'</p>';
		return $html;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function init($content, $title = null)
	{
		$model = new TextFieldModel();
		$model->setContent($content);
		$model->setTitle($title);
		$this->model = $model;
	}
}




