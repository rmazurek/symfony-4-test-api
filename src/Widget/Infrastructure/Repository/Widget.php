<?php

// src/Widget/Infrastructure/Repository/Banner.php
namespace App\Widget\Infrastructure\Repository;

use App\Widget\Domain\Repository\WidgetInterface;

abstract class Widget implements WidgetInterface
{
	/**
	 * @var App\Widget\Domain\Model\Widget $data
	 *
	 * Widget data model.
	 */
	protected $data = null;
	
	/**
	 * {@inheritdoc}
	 */
	public function getData()
	{
		return $this->data;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function getShortCode()
	{
		return $this->model->getShortCode();
	}
}
