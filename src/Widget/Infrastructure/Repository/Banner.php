<?php

// src/Widget/Infrastructure/Repository/Banner.php
namespace App\Widget\Infrastructure\Repository;

use App\Widget\Domain\Model\Banner as BannerModel;
use App\Widget\Domain\Repository\BannerInterface;
use App\Widget\Infrastructure\Repository\Widget;

/**
 * Banner widget class.
 */
class Banner extends Widget implements BannerInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function render()
	{
		$model = $this->data;
		$html = '';
		if($model->getTitle()){
			$html .= '<p>'.$model->getTitle().'</p>';
		}
		$html .= '<a href="'.$model->getUrl().'">';
		$html .= '<img src="'.$model->getImage()->getUrl().'"/>';
		$html .= '</a>';
		return $html;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function init($url, $imageUrl, $title = null)
	{
		$model = new BannerModel();
		$model->setUrl($url);
		if(isset($_SERVER['PWD'])){
			$imageUrl = $_SERVER['PWD'].'/public/images/'.$imageUrl;
		}else{
			$imageUrl = $_SERVER['DOCUMENT_ROOT'].'/images/'.$imageUrl;
		}
		if(file_exists($imageUrl)){
			$model->setImage($imageUrl);
		}else{
			die('No such file');
		}
		$model->setTitle($title);
		$this->data = $model;
	}
}
