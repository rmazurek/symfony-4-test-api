<?php

// src/Widget/Domain/Repository/BannerInterface.php
namespace App\Widget\Domain\Repository;

use App\Widget\Domain\Repository\WidgetInterface;

interface BannerInterface extends WidgetInterface
{
	/**
	 * Init banner parameters.
	 * 
	 * @param string $url banner url
	 * @param string $imageUrl banner image url
	 * @param string $title optional banner title
	 * 
	 * @return void
	 */
	public function init($url, $imageUrl, $title = null);
}
