<?php

// src/Widget/Domain/Repository/TextFieldInterface.php
namespace App\Widget\Domain\Repository;

use App\Widget\Domain\Repository\WidgetInterface;

interface TextFieldInterface extends WidgetInterface
{
	/**
	 * Init field parameters.
	 * 
	 * @param string $contet text content
	 * @param string $title optional title
	 * 
	 * @return void
	 */
	public function init($content, $title = null);
}
