<?php

// src/Widget/Domain/Repository/WidgetInterface.php
namespace App\Widget\Domain\Repository;

interface WidgetInterface
{
	/**
	 * Get data model.
	 * 
	 * @return Domain\Model\Widget
	 */
	public function getData();
	
	/**
	 * Get short code.
	 * 
	 * @return string
	 */
	public function getShortCode();
	
	/**
	 * Render HTML string.
	 * 
	 * @return string
	 */
	public function render();
}


