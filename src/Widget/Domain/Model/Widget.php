<?php

// src/Widget/Domain/Model/Widget.php
namespace App\Widget\Domain\Model;

/**
 * General widgets class.
 * 
 * Basic widgets functions.
 * All widgets types inherit this class.
 */
abstract class Widget
{
	/**
	 * @var string $shortCode
	 * 
	 * Widget identifier string.
	 */
	protected $shorCode = '';
	
	/**
	 * Set short code.
	 * 
	 * @param string $shortCode
	 * 
	 * @return void
	 */
	public function setShortCode($shortCode)
	{
		$this->shortCode = $shortCode;
	}
	
	/**
	 * Get short code.
	 * 
	 * @return string
	 */
	public function getShortCode()
	{
		return $this->shortCode;
	}
}
