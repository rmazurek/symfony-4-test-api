<?php

// src/Widget/Domain/Model/TextField.php
namespace App\Widget\Domain\Model;

use Symfony\Component\Validator\Constraints as Assert;
use App\Widget\Domain\Model\Widget;

class TextField extends Widget
{
	/**
	 * @var string $content
	 * 
	 * Text content.
	 * 
	 * @Assert\NotNull()
	 * @Assert\Length(min = 3)
	 */
	protected $content = '';
	
	/**
	 * @var string $title
	 * 
	 * Text title.
	 * 
	 * @Assert\Length(max = 100)
	 */
	protected $title = '';
	
	/**
	 * Set content.
	 * 
	 * @param string $content text content
	 * 
	 * @return void
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}
	
	/**
	 * Get content.
	 * 
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}
	
	/**
	 * Set title.
	 * 
	 * @param string $title
	 * 
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Get title.
	 * 
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}
}
