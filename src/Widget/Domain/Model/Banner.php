<?php

// src/Widget/Domain/Model/Banner.php
namespace App\Widget\Domain\Model;

use Symfony\Component\Validator\Constraints as Assert;
use App\Widget\Domain\Model\Image;
use App\Widget\Domain\Model\Widget;

/**
 * Banner widget class.
 * 
 * Banners are links displayed as images.
 */
class Banner extends Widget
{
	/**
	 * @var App\Widget\Domain\Model\Image $image
	 *
	 * Image object
	 */
	protected $image = null;
	
	/**
	 * @var string $title
	 * 
	 * Banner title.
	 * 
	 * @Assert\Length(max = 100)
	 */
	protected $title = '';
	
	/**
	 * @var string $url
	 * 
	 * Banner's url.
	 * 
	 * @Assert\NotNull()
	 * @Assert\Length(min = 3)
	 */
	protected $url = '';

	/**
	 * Constructor class.
	 * 
	 * Set widget shortcode.
	 */
	public function __construct()
	{
		$this->shortCode = 'banner';
	}
	
	/**
	 * Set image for banner.
	 *
	 * @param string $url url for image file
	 *
	 * @return void
	 */
	public function setImage($url)
	{
		$image = new Image();
		$image->setUrl($url);
		$this->image = $image;
	}
	
	/**
	 * Get banner's image object.
	 *
	 * @return App\Widget\Domain\Model\Image
	 */
	public function getImage()
	{
		return $this->image;
	}
	
	/**
	 * Set banner's title.
	 * 
	 * @param string $title
	 * 
	 * @return void
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	/**
	 * Get banner's title.
	 * 
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
	/**
	 * Set banner url.
	 * 
	 * @param string $url url string
	 * 
	 * @return void
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}
	
	/**
	 * Get banner url.
	 * 
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}
}

