<?php

// src/Widget/Domain/Model/Image.php
namespace App\Widget\Domain\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Images class.
 */
class Image
{
	/**
	 * @var string $url
	 * 
	 * Url to image file.
	 * 
	 * @Assert\NotNull
	 */
	protected $url;
	
	/**
	 * Set image url.
	 * 
	 * @return void
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}
	
	/**
	 * Get image url.
	 * 
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}
}